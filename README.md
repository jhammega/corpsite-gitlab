# CorpSite GitLab version

This is a demo of a corporate website using GitLab.

The goal is to show changes  to this app that propagate through the DevOps app.

# Setup on runner instance
runnerssh

Xvfb :10 -ac &
export DISPLAY=:10
